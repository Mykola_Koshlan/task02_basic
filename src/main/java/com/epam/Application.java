package com.epam;

import java.util.Scanner;

/**
 *
 */
public class Application {

    public static void main(String[] args) {
        int start;
        int end;
        int size;
        Scanner input = new Scanner(System.in);
        System.out.print("Input first number: ");
        start = input.nextInt();
        System.out.print("Input second number: ");
        end = input.nextInt();
        doOddIncrease(start, end);
        doEvenDecrease(start,end);
        System.out.println("\nEnter the size of Fibonacci set ");
        size = input.nextInt();
        doFibonacci(size);

    }

    /**
     * @param size size of fibonacci array
     */
    private static void doFibonacci(int size) {
        int oddMax = 1;
        int evenMax = 0;
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= size; i++){
            n2=n0+n1;
            if(n2%2==0){
                evenMax = n2;
            }
            if(n2%2==1){
                oddMax = n2;
            }
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;
        }
        System.out.println();
        System.out.println("F1 = "+oddMax);
        System.out.println("F2 = "+evenMax);

    }

    /**
     * @param start
     * @param end
     */
    private static void doOddIncrease(int start, int end) {
        System.out.println("Odd numbers from " + start + " to " + end);
        for (int i = start; i <= end; i += 2) {
            if ((i % 2) == 0) {
                i++;
            }
            System.out.print(i + " ");
        }
    }
    private static void doEvenDecrease(int start, int end) {
        System.out.println("\nEven numbers from " + end + " to " + start);
        for (int i = end; i >= start; i -= 2) {
            if ((i % 2) == 1) {
                i--;
            }
            System.out.print(i + " ");
        }
    }

}









